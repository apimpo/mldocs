# Model Serving

Model serving represents making a **trained** model available to the users and other software components. 
  
The goal of model serving is to be able to query the model, to send data inputs and obtain model outputs (predictions).  
  
Models can be queried directly from the applications, for example TensorFlow `model.predict()` function. The limitation of this approach is that every user or application needs access to the stored model (architecture + parameters), making this solution unscalable for a system with multiple users and applications.  
  
Another option is **API Model Serving**, which exposes a model with a REST API.

## API Model Serving

The idea of API model serving is to expose a model via REST API.  
  
A model server with the access to the model's parameters and architecture is created.  
To query the model, only IP and model endpoint are needed, for example:  
`curl -v "http://MODEL_IP:PORT/v1/models/custom-model:predict" -d @./input.json`

## Autoscaling

Scaling is an important aspect of model serving. With Kubeflow, it is possible to configure a *serverless* infrastructure, so that the number of server instances (replicas) increases with the number of requests to the model's API.

## Workflow

Model serving is simplified with Kubeflow, requiring two main steps:  

* Store a trained model in a persistent storage location. Storage can be:  
    * S3 bucket, world accessible or with credentials - [example](https://github.com/kubeflow/kfserving/blob/master/docs/samples/storage/s3/tensorflow_s3.yaml).  
    * Kubernetes [PVC](https://kubernetes.io/docs/concepts/storage/persistent-volumes/), which is cluster-specific - [example](https://github.com/kubeflow/kfserving/blob/master/docs/samples/storage/pvc/mnist-pvc.yaml).  
    * Uri, which could be a github location - [example](https://github.com/kubeflow/kfserving/blob/master/docs/samples/storage/uri/tensorflow.yaml).  
* Create an InferenceService [CRD](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/), to deploy a model server. Define _yaml_ file with: 
    * A name of the service.  
    * Location from which to obtain the model for serving (from the previous step).  
    * Resources, for example GPUs - [example](https://github.com/kubeflow/kfserving/blob/master/docs/samples/accelerators/tensorflow-gpu.yaml).  

## GPU Drivers

If you rely on GPUs for your inference servers, it is likely the required
drivers are not available inside the image. You can instruct the system to make
them available with a simple label:
```yaml
apiVersion: "serving.kubeflow.org/v1alpha2"
kind: "InferenceService"
metadata:
  name: "tensorflow-gpu"
  labels:
    nvidia-drivers: "true"
spec:
  default:
    predictor:
      tensorflow:
        storageUri: "s3://BUCKET"
        runtimeVersion: "1.11.0-gpu"
        resources:
          limits:
            nvidia.com/gpu: 1
```

## Frameworks

Model serving is supported for major ML frameworks, such as TensorFlow, PyTorch or SKLearn.  
Support is provided for XGBoost, LightGBM, PMML and ONNXRuntime, among others.  
  
More information is provided on [kfserving samples documentation page](https://github.com/kubeflow/kfserving/blob/master/docs/samples/README.md#kfserving-features-and-examples).   

## Examples

TensorFlow and PyTorch examples use already trained models from KFServing github repository.  
For training and serving a custom model, please refer to [custom model](#custom) example.  

### Single Model
* Connect to a running Notebook server
    * If needed, follow [the guide for setting up Jupyter notebooks in Kubeflow](https://ml.docs.cern.ch/notebooks/)
* In the Notebook terminal clone [git repository](https://gitlab.cern.ch/ai-ml/examples) with examples
```
git clone https://gitlab.cern.ch/ai-ml/examples.git
```
* Follow the instructions from here - https://gitlab.cern.ch/ai-ml/examples/serving/single-model/README.md

### Multi Model Serving
* Connect to a running Notebook server
    * If needed, follow [the guide for setting up Jupyter notebooks in Kubeflow](https://ml.docs.cern.ch/notebooks/)
* In the Notebook terminal clone [git repository](https://gitlab.cern.ch/ai-ml/examples) with examples
```
git clone https://gitlab.cern.ch/ai-ml/examples.git
```
* Follow the instructions from here - https://gitlab.cern.ch/ai-ml/examples/serving/multi-model/README.md

### Custom

Few steps are needed to serve a custom model:  

* Create a bucket at s3.cern.ch using [documentation](https://clouddocs.web.cern.ch/object_store/s3cmd.html)
    * Make sure the credentials are properly obtained
* Connect to a running Notebook server
    * If needed, follow [the guide for setting up Jupyter notebooks in Kubeflow](https://ml.docs.cern.ch/notebooks/)
* In the Notebook terminal clone [git repository](https://gitlab.cern.ch/ai-ml/examples) with examples
```
git clone https://gitlab.cern.ch/ai-ml/examples.git
```
* Navigate to **examples/serving/custom_tensorflow** in a Notebook server
* Fill in `credentials` file with your bucket credentials
* Create `~/.aws` directory
```
mkdir ~/.aws
```
* Copy credentials file to `~/.aws` directory
```
cp credentials ~/.aws
```
* Train the model using [model_training.ipynb](https://gitlab.cern.ch/ai-ml/examples/-/blob/master/serving/custom/model_training.ipynb) notebook
    * Make sure `bucket` variable corresponds to the bucket created in the first step
    * After running the notebook, model should be available in the bucket

* Fill in storage uri and bucket credentials in InferenceService definition files:  
    * CPU serving [storage uri](https://gitlab.cern.ch/ai-ml/examples/-/blob/master/serving/custom/custom_model_cpu.yaml#L10) and [bucket credentials](https://gitlab.cern.ch/ai-ml/examples/-/blob/master/serving/custom/custom_model_cpu.yaml#L21)
    * GPU serving [storage uri](https://gitlab.cern.ch/ai-ml/examples/-/blob/master/serving/custom/custom_model_gpu.yaml#L12) and [bucket credentials](https://gitlab.cern.ch/ai-ml/examples/-/blob/master/serving/custom/custom_model_gpu.yaml#L32)

* Create an InferenceService using [CPU](https://gitlab.cern.ch/ai-ml/examples/-/blob/master/serving/custom/custom_model_cpu.yaml) or [GPU](https://gitlab.cern.ch/ai-ml/examples/-/blob/master/serving/custom/custom_model_gpu.yaml):  
    * CPU: `kubectl apply -f custom_model_cpu.yaml`
    * GPU: `kubectl apply -f custom_model_gpu.yaml`

* Check the status of InferenceService with: `kubectl get inferenceservice`

* Obtain an Authentication Session Cookie from Chrome
    * Click `View -> Developer -> Developer Tools -> Network`
    * Navigate to [ml.cern.ch](https://ml.cern.ch)
    * Check Request Headers
        * Copy section `authservice_session`

* Run Inference
    * AUTH_SESSION is the authentication session cookie obtained in the previous step
    * NAMESPACE is a personal Kubeflow namespace, which can be seen in the top left corner of the UI
    * ```curl -H 'Cookie: authservice_session=AUTH_SESSION' -H 'Host: custom-model-cpu-predictor-default.NAMESPACE.example.com' http://ml.cern.ch/v1/models/custom-model-cpu:predict -d @./input.json```

* Expected output  
```
{
    "predictions": [[0.779803514]
    ]
}
```

### Additional

Additional examples are provided at the [kserve samples page](https://github.com/kserve/kserve/tree/release-0.6/docs/samples).  
