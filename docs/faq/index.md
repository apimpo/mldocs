### How do I connect to [ml.cern.ch](https://ml.cern.ch) outside CERN?

The service is not yet open in the CERN firewall.

Follow one of the documented recipes for remote access to CERN services in [Teleworking Tips & Tricks](https://codimd.web.cern.ch/vjC8BHbTS7etHwJve-K2Uw#Remote-Access).

### I get a permission error while accessing EOS

`PermissionError: [Errno 13] Permission denied: '/eos/user/...`

Refresh your kerberos token with:
```
kinit YOURID@CERN.CH
```

Soon this will be managed automatically and renewal won't be required.

### My Notebook server is stuck during creating (longer than 10 minutes)

Feedback when notebook launch fails is not ideal, if this persists please
contact us in the [ML](https://mattermost.web.cern.ch/it-dep/channels/ml)
mattermost channel of IT-dep team.
