# Notebooks

To provide full data science experience, ml.cern.ch offers Jupyter Notebooks.  
Every Notebook server can be customized, by installing required packages.  
For every Notebook server, resources such as CPUs or GPUs can be added.  

<a name="create"></a>To create a Notebook server:

* Go to [https://ml.cern.ch/_/jupyter/](https://ml.cern.ch/_/jupyter/) .  
* Select **New Server** .  
* Select a name that starts with a letter (not a number).  
* Select an image.  
    * From the list of [base images](#base-images).  
    * Use a [custom image](#custom-images).  
* Select the number of CPUs and amount of Memory.  
* Select the number of GPUs. **Please be aware that a GPU might not be available**.   
* If a persistent storage is needed, select the size.  
    * This storage is only intended for keeping the notebook state, in case of stopping the server.   
    * Do not use this storage for large datasets (> 5 GB), use EOS or S3 instead.  
* Select default configurations.  
    * Allow access to Kubeflow Pipelines. 
    * Mount EOS.  
    * Mount Nvidia driver (important if you have selected a GPU).   
* Wait until notebook server starts, then click **Connect** .  

## Base Images

The fastest way to start development is to select one of pre-built images to run a Jupyter notebook.  

Pre-built images contain most functionalities to provide machine learning training and integrate with existing CERN services.  

Pre-built images contain:  

* Python 3.  
* TensorFlow or Pytorch.  
* Common Python libraries: matplotlib, boto3, sklearn
* kfp library for intergration with Kubeflow pipelines

The repo for building base images can be viewed here: https://gitlab.cern.ch/ai-ml/kubeflow_images/-/tree/cern_14
    
If additional functionalities or libraries are required:  

* They can be installed in the running server with: `pip3 install LIBRARY --user` .  
* Or [custom image](#custom-images) can be built.  

## Custom Images

Custom images offer a possibility to create a customized machine learning environment.  
This can be useful for Notebooks, but is especially useful for Pipelines and Katib, where it's not possible to install with pip.

Custom images can be built in 2 ways, manual and automatic (Gitlab CI/CD).  
Manual build is better for beginners with Docker, to get familiar with the technology.  
In addition, manual builds are better for fast iteration (often small changes in the Dockerfile).  
Autobuild option is better when the image has reached the production stage.  

### Manual Build

* Clone [git repository](https://gitlab.cern.ch/ai-ml/custom_ml_images/) with a basic Dockerfile:  
```
git clone https://gitlab.cern.ch/ai-ml/custom_ml_images.git
```

* Position to the directory:  
```
cd custom_ml_images
```

* Edit _requirements.txt_ to add or remove desired packages.  
* Edit _Dockerfile_, in case other actions are needed.  
* Build Docker image:  
```
docker build . -f Dockerfile -t gitlab-registry.cern.ch/ai-ml/CUSTOM_REPO_PATH
```

* Push built custom image to a container registry:  
```
docker push gitlab-registry.cern.ch/ai-ml/CUSTOM_REPO_PATH
```
    
* Create the new Notebook Server by following steps [here](#create)


### Autobuild

* Fork [git repository](https://gitlab.cern.ch/ai-ml/custom_ml_images/).   
* Edit FROM tag in Dockerfile to set the base image.    
* Edit Dockerfile and requirements.txt for custom packages.   
* Check GitLab left panel - CI/CD - Pipelines.   
    * With every push to this repo, a pipeline is created, docker image is being built.     
* After the pipeline completion, check Packages & Registries - Container registry.   
    * Copy the tag of the docker image.   
