# ML Project

The project aims at offering a centralized service to manage the full machine
learning lifecycle:

* Data Extraction and Preparation
* Interactive analysis and iteration
* Distributed Training, Hyper Parameter Optimization
* Model Storage, Versioning and Serving

It also offers access to a large amount of accelerator resources like GPUs,
TPUs, IPUs and FPGAs - both on premises and external.

## Meetings

We run bi-weekly meetings for sprints and discussion.

* Every Second Monday 11am - [events here](https://indico.cern.ch/category/14173/)
* [Kanban Board](https://its.cern.ch/jira/secure/RapidBoard.jspa?rapidView=7376)

## Communication

* [Mattermost](https://mattermost.web.cern.ch/it-dep/channels/ml)

## Milestones

### Major


### Ongoing Work

* [Improving GPU allocation in the cluster](https://gitlab.cern.ch/groups/ai-ml/-/epics/1)
* [Automation of Credential Renewal](https://its.cern.ch/jira/browse/OS-13612)

## Presentations

### Internal

* CMS Group Meeting, Jun 24 2022: <a href="https://gitlab.cern.ch/ai-ml/mldocs/-/blob/master/docs/project/resources/gnn-jec-kubeflow.pdf">gnn-jec-kubeflow.pdf</a>
* CMS Knowledge Group Meeting, Feb 28 2022: [Kubeflow at CERN](https://indico.cern.ch/event/1127775/)
* ATLAS CAT Physics Weekly, Feb 09 2022: [ML Lifecycle in CERN IT](https://indico.cern.ch/event/1112707/)
* IT Technical Forum, Nov 19 2021: [Centralized Management of Your Machine Learning Lifecycle: Preparation, Training and Model Serving](https://indico.cern.ch/event/1083485/)
* Machine Learning Coffees, Oct 16 2020: [Making ML easier with Kubeflow](https://indico.cern.ch/event/966345/)
* Joint AMG and WFMS Meeting on Analysis Facilities, Sep 24 2021: [Kubeflow Overview](https://indico.cern.ch/event/1071685/)
* DUNE, Oct 02 2020: [Kubeflow Overview and Demo](https://indico.cern.ch/event/961276/)

### External


* Google Summer of Code 2022, July 29 2022: [Geant4-FastSim - Building an ML pipeline for Fast Shower Simulation](https://hepsoftwarefoundation.org/gsoc/blogs/2022/blog_Geant4_GuneetSingh.html)
* Google Summer of Code 2022, July 27 2022: [Geant4-FastSim - Memory Footprint Optimization for ML Fast Shower Simulation](https://hepsoftwarefoundation.org/gsoc/blogs/2022/blog_Geant4_PriyamMehta.html)
* Kubecon Europe 2022, May 19 2022: [Jet Energy Corrections with GNN Regression using Kubeflow @ CERN](https://kccnceu2022.sched.com/event/ytqv/jet-energy-corrections-with-gnn-regression-using-kubeflow-at-cern-daniel-holmberg-dejan-golubovic-cern)
* Kubernetes AI Day North America, Oct 12 2021: [A Better and More Efficient ML Experience for CERN Users](https://kubernetesaidayna21.sched.com/event/mGNA?iframe=no)
* Kubecon Europe 2021, May 4-7 2021: [Building and Managing a Centralized ML Platform with Kubeflow at CERN](https://kccnceu2021.sched.com/event/iE1w/building-and-managing-a-centralized-ml-platform-with-kubeflow-at-cern-ricardo-rocha-dejan-golubovic-cern?iframe=no&w=100%&sidebar=yes&bg=no)
* Fast Machine Learning for Science Workshop, Dec 01 2020: [Making ML Easier with Kubeflow](https://indico.cern.ch/event/924283/contributions/4105328/)
* 25th International Conference on Computing in High-Energy and Nuclear Physics, May 17-21 2020: [Training and Serving ML workloads with Kubeflow at CERN](https://indico.cern.ch/event/948465/contributions/4323970/)

## References

* [Upstream Documentation](https://www.kubeflow.org/)
