# Pipelines

Machine Learning Pipelines define work-flows in the form of a directed graph.  
Pipelines provide more flexibility to define dependencies between components compared to classic scripts or notebooks.  
For example, with pipelines, multiple models can be trained in parallel or data can be read from multiple streams in parallel.

<video width="427" height="240" controls>
  <source src="https://indico.cern.ch/event/924283/contributions/4105328/attachments/2153724/3632746/Kubeflow%20Demo%20Pipelines.mp4" type="video/mp4">
</video>

## Recurrent

Sometimes, it's needed to have a recurring machine learning job. For example model training with new data every day, or periodic monitoring of services availability, or monitoring usage of a specific system. To do so, a recurrent pipeline can be submitted, specifying various pipeline triggering option, such period between two pipeline runs, start and end date/time or cron options.

## Pipeline Parameters
Every pipeline can have input values that can change every time the pipeline is triggered. 
For example, input data paths, or different options for hyperparameters can be supplied as pipeline parameters, rather than hard coded.  
By default, pipelines are run without any parameters.

## Running a Pipeline

### Hello World - YAML file

To run pipelines from pipeline dashboard, a .yaml file with pipeline definition is needed. It can be a custom pipeline, or one of the provided examples.

* Navigate to https://ml.cern.ch/_/pipeline/
* Click Upload Pipeline
* Download [the example yaml file](https://gitlab.cern.ch/ai-ml/examples/-/blob/master/argo-workflows/helo-world/dag_diamond.yaml)
* Add Pipeline name
* Upload the downloded [example](https://gitlab.cern.ch/ai-ml/examples/-/blob/master/argo-workflows/helo-world/dag_diamond.yaml)
* Click Create
* Click **Create Run**
    * Select Run name
    * Select Experiment in which to run the pipeline. An experiment holds multiple runs of the same pipeline, and make it easier to track the runs. If needed, create an Experiment first.
    * Select if the pipeline is run once, or is [recurrent](#recurrent)
      * If recurrent, select pipeline trigger options (period, start and end date/time)
    * Select [Pipeline Parameters](#pipeline-parameters)
    * Click **Start**
* Click on the name of the running pipeline (Run name)
* Track the progress of the pipeline

### MNIST - Kale
* Follow [the guide to setting up Jupyter notebooks in Kubeflow](https://ml.docs.cern.ch/notebooks/)
    * Select image `registry.cern.ch/ml/kf-14-tensorflow-jupyter-kale:v1`
* In the Notebook terminal clone [git repository](https://gitlab.cern.ch/ai-ml/examples) with examples
```
git clone https://gitlab.cern.ch/ai-ml/examples.git
```
* Open **examples/pipelines/kale/mnist-kale-katib.ipynb** in the Notebook server
* On the left side of the Notebook panel, select Kubeflow Pipelines Deployment Panel
* Toggle **Enable**
* Select Experiment (existing or new)
* Write Pipeline name and Pipeline description
* Untoggle **HP Tuning with Katib**
* Click **Compile and Run** at the bottom of the page
* After successfull compilation, click View
* Inspect and debug your pipeline via Pipeline log


### Access EOS - kfp library

* Connect to a running notebook server - [ml.cern.ch/_/jupyter/](https://ml.cern.ch/_/jupyter/)
    * If needed, follow [the guide to setting up Jupyter notebooks in Kubeflow](https://ml.docs.cern.ch/notebooks/)
* Open a new Terminal instance
* In Terminal, clone [git repository](https://gitlab.cern.ch/ai-ml/examples) with examples
```
git clone https://gitlab.cern.ch/ai-ml/examples.git
```
* Navigate to **examples/pipelines/argo-workflows/access_eos**
* Login to kerberos with kinit:
```
kinit CERN-USER-ID
```
* When kerberos has been refreshed, remove any old secret before creating a new one
```
kubectl delete secret krb-secret
```
* Create a kerberos secret for Kubernetes.  
```
kubectl create secret generic krb-secret --from-file=/tmp/krb5cc_1000
```
* Customize `access_eos.yaml` file.  
    * Change user directory in EOS.  
    * Use custom image if needed.  
    * Use custom command and scripts.  
* Upload the pipeline
```
kfp pipeline upload -p eos_secrets_pipeline access_eos.yaml
```
* Create an experiment (a wrapper for pipeline runs).  
```
kfp experiment create argo_experiment
```
* Run the pipeline.  
```
kfp run submit -e argo_experiment -n eos_secrets_pipeline -r first_run -w
```
* Monitor pipeline completion from Terminal.  
    * Or use UI to explore pipeline logs - https://ml.cern.ch/_/pipeline/#/experiments  

### End-to-end Jet Energy Correction Pipeline
* A full example can be found here: https://gitlab.cern.ch/dholmber/jec-pipeline
