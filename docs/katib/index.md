# Hyperparameter Optimization with Katib

Hyperparameter optimization is a task of finding the **set of non-trainable parameters** for which the model reaches best performance. Hyperparameters include learning rate, number of layers, number of nodes in each layer, size of convolutional layers, different activation functions, and many other options.  

Hyperparameter optimization is an important task in machine learning, which usually takes a lot of effort when implementing by hand. [Katib](https://www.kubeflow.org/docs/components/katib/overview/) is a component of ml.cern.ch that offers an **automated** way for the hyperparameter optimization.

<video width="427" height="240" controls>
  <source src="https://indico.cern.ch/event/924283/contributions/4105328/attachments/2153724/3632745/Kubeflow%20Demo%20Katib.mp4">
</video>

## Katib Concepts

### Experiment

An experiment is a single tuning run, also called an optimization run.  
Configuration settings are specified define the experiment. Main configurations are:

* **Objective** - metrics that needs to be optimized, such as accuracy or loss. It can be specified if the metrics needs to be maximized or minimized.  
* **Search space** - the set of all possible hyperparameter values that the hyperparameter tuning job should consider for optimization, and the constraints for each hyperparameter.  
* **Search algorithm** - The algorithm to use when searching for the optimal hyperparameter values.

### Trial

A trial is one iteration of the hyperparameter tuning process.  
Each experiment runs several trials. The experiment runs the trials until it reaches either the objective or the configured maximum number of trials.

## Running Katib examples

### From a Notebook

* Clone [git repository](https://gitlab.cern.ch/ai-ml/examples) with examples
```
git clone https://gitlab.cern.ch/ai-ml/examples.git
```
* Open **mnist-kale/mnist-kale-katib.ipynb** in a Notebook server
* On the left side of the Notebook panel, select Kubeflow Pipelines Deployment Panel
* Toggle **Enable**
* Select Experiment (existing or new)
* Write Pipeline name and Pipeline description
* Toggle **HP Tuning with Katib**
* Click **SET UP KATIB JOB**
* Select options to run the search
    * **Search Space Parameters** - actual hyperparameters and their values
    * **Search Algorithm** (grid, random, Bayesian...)
    * **Search Objective**
        * *Reference Metrics* to optimize towards (test-accuracy, train-loss, etc)
        * *Minimize or maximize* the given metrics (loss - minimize, accuracy - maximize)
        * *Goal* - if the goal is reached, the experiment stops, the remaining trials are not run
    * **Run Parameters**
        * *Parallel Trial Count* - number of trials that would run in parallel. This should correspond to the number of available GPU-s.  More trials run in parallel means the whole job will complete sooner.
        * *Max Trial Count* - in general, maximum number of trials is determined by the number of possible combinations of selected hyperparameters. If we want to limit this number to be less than theorethical maximum, max trial count can be selected. **The experiment stops for three reasons: all combinations are tested, maximum trial count is reached or the goal is achieved**.
        * *Max Failed Trial Count* - experiments can fail because of the bugs or the system errors. If we want to tolarate failure of a certain number of trials before declaring the entire experiment as a failure, this option can be selected. **In general, the failed trial count should be 1**.
* Click **Compile and Run** at the bottom of the page
* After successfull compilation, click View
* Inspect the running Katib Job

### From the Katib Dashboard

Running Katib experiments from Katib dashboard requires:

* Built **Docker image** with all dependencies to run the training script.
* **Training script**, implemented to read hyperparameters as command line argumements.
* **.yaml** file to define the experiment and trials.

To get started:

* Navigate to https://ml.cern.ch/_/katib/      
* Click `NEW EXPERIMENT`.   
* At the bottom of the page click `Edit and submit YAML`.   
* Copy the content of the file: https://gitlab.cern.ch/ai-ml/examples/-/blob/kubeflow_1.4.0/katib/hp-tuning.yaml to the text editor.   
* Click `CREATE`.  
* The progress can be monitored in the UI on https://ml.cern.ch/_/katib/experiment.  

For more info, checkout the Katib example page - https://gitlab.cern.ch/ai-ml/examples/-/tree/kubeflow_1.4.0/katib
