# Model Storage

To store and version information about trained models, such as architecture and parameters, there are different options.  
Persistant file system model storage can be obtained via [EOS](https://eos-web.web.cern.ch/eos-web/).  
Other services discussed on this page provide additional functionalities, such as object storage and model versioning. These functionalities can be utilized to facilitate a model registry system, per user requirements.   

## EOS  
[EOS](https://eos-web.web.cern.ch/eos-web/) provides a service for storing large amounts of physics data and user files.  
EOS can be used as a file system to store any data, including machine learning models.

## Cluster Object Storage

Kubeflow provides internal object storage system.  
[Minio](https://min.io/) is a Kubernetes native [object storage](https://en.wikipedia.org/wiki/Object_storage) used in cloud computing. Minio buckets can be used to store any data, including models. Python API reference can be found [here](https://docs.min.io/docs/python-client-quickstart-guide.html).  
**Note** - Due to the ephemeral nature of containerized systems, it is important to note that data stored on Minio buckets **can be lost**.  
In addition, the in-cluster minio buckets are accessible to all users.  
It is suggested to use minio as a **starting point**, before moving to more persistant solutions, such as [CERN object storage](https://clouddocs.web.cern.ch/object_store/README.html) - s3.cern.ch.

## CERN Object Storage

[s3.cern.ch](https://clouddocs.web.cern.ch/object_store/README.html) offers an object storage system compatible with [Amazon S3](https://en.wikipedia.org/wiki/Amazon_S3) protocol.  
Created buckets on s3.cern.ch can be accessed from anywhere, if credentials are in place.  
Guide on how to get started with s3.cern.ch is provided [here](https://clouddocs.web.cern.ch/object_store/README.html).  
Python code to upload a file to the existing bucket is provided below:

```
import boto3

client = boto3.client('s3', endpoint_url='https://s3.cern.ch')
client.upload_file(LOCAL_FILEPATH, BUCKET_NAME, FILENAME_ON_BUCKET)
``` 

## CERN Registry
**Experimental**, fully OCI compliant registry, is available at [registry.cern.ch](https://registry.cern.ch).  
The official Harbor documentation can be found [here](https://goharbor.io/docs/2.1.0/working-with-projects/).  
To interact with CERN registry to store ML models, [ormb tool](https://github.com/kleveross/ormb).  
ormb can be used to create, version, share and publish models.

## Example

* Clone [git repository](https://gitlab.cern.ch/ai-ml/examples) with examples
```
git clone https://gitlab.cern.ch/ai-ml/examples
```
* Open notebook `examples/storage/model-storage.ipynb`
    * This notebook containes examples on how to store models via EOS, minio, s3.cern.ch and CERN registry.
* Follow the instructions and run all the cells



